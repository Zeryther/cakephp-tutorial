<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\Query;
use Cake\Utility\Text;
use Cake\Validation\Validator;

class ArticlesTable extends Table {
    public function beforeSave($event, $entity, $options){
        if($entity->isNew()){
            $sluggedTitle = Text::slug($entity->title);
            $entity->slug = substr($sluggedTitle,0,191);
        }
    }

    protected function _buildTags($tagString){
        $newTags = array_unique(array_filter(array_map("trim",explode(",",$tagString))));

        $out = [];
        $query = $this->Tags->find()
            ->where(["Tags.title IN" => $newTags]);

        foreach($query->extract("title") as $existing){
            $index = array_search($existing,$newTags);
            if($index !== false){
                unset($newTags[$index]);
            }
        }

        foreach($query as $tag){
            $out[] = $tag;
        }

        foreach($newTags as $tag){
            $out[] = $this->tags->newEntity(["title" => $tag]);
        }

        return $out;
    }

    public function findTagged(Query $query, array $options){
        $columns = [
            "Articles.id","Articles.user_id","Articles.title","Articles.body","Articles.published","Articles.created","Articles.slug"
        ];

        $query = $query
            ->select($columns)
            ->distinct($columns);

        if(empty($options["tags"])){
            $query->leftJoinWith("Tags")
                ->where(["tags.title IS" => null]);
        } else {
            $query->innerJoinWith("Tags")
                ->where(["Tags.title IN" => $options["tags"]]);
        }

        return $query->group(["Articles.id"]);
    }

    public function initialize(array $config){
        $this->addBehavior("Timestamp");
        $this->belongsToMany("Tags");
    }

    public function validationDefault(Validator $validator){
        $validator
            ->notEmpty("title")
            ->minLength("title", 10)
            ->maxLength("title", 255)

            ->notEmpty("body")
            ->minLength("body", 10);

        return $validator;
    }
}